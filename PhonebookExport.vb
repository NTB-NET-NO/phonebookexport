Imports System.Xml
Imports System.Net
Imports System.Text
Imports System.IO
Imports System.Configuration.ConfigurationSettings

Module PhonebookExport

    'Vars
    Dim sessionId As String
    Dim userId As String

    'Logfile
    Dim logFile As StreamWriter

    'Pages
    Dim loginPage As String = AppSettings("loginPage")
    Dim uploadPage As String = AppSettings("uploadPage")
    Dim uploadTargetPage As String = AppSettings("uploadTargetPage")


    Sub Login(ByVal str As String)

        'Login: get session id

        Dim request As HttpWebRequest = WebRequest.Create(loginPage & str)
        Dim response As HttpWebResponse

        Try
            response = request.GetResponse()
        Catch
            logFile.WriteLine(LogTimeStamp() & "Connection error: Unable to connect to web site '" & loginPage & str & "'")
        End Try

        Dim reader As StreamReader = New StreamReader(response.GetResponseStream())
        Dim data As String = New String(reader.ReadToEnd())
        reader.Close()

        Dim s As Integer = data.IndexOf("sessionid=") + 10
        Dim e As Integer = data.IndexOf("&", s)
        sessionId = data.Substring(s, e - s)

        'Upload page: get user id
        request = WebRequest.Create(uploadPage & sessionId)

        Try
            response = request.GetResponse()
        Catch
            logFile.WriteLine(LogTimeStamp() & "Connection error: Unable to log in to web site '" & uploadPage & sessionId & "'")
        End Try


        reader = New StreamReader(response.GetResponseStream())
        data = reader.ReadToEnd()
        reader.Close()

        s = data.IndexOf("edituser value=") + 15
        e = data.IndexOf(">", s)
        userId = data.Substring(s, e - s)

        logFile.WriteLine(LogTimeStamp() & "Status information: Logged in to web site, ready to upload phonelist")

    End Sub

    Sub UploadList()

        'Compile the data to send
        logFile.WriteLine(LogTimeStamp() & "Status information: Creating multipart/form-data to send")

        Dim formData As String = New String("")
        formData = "--AaB03x" & vbCrLf & "Content-Disposition: form-data; name=""userfile""; filename=""tab-list.txt""" & vbCrLf & "Content-Type: text/plain" & vbCrLf & vbCrLf

        Dim reader As StreamReader = New StreamReader(AppSettings("tmpList"), Encoding.GetEncoding("iso-8859-1"))
        formData &= reader.ReadToEnd()
        reader.Close()

        formData &= "--AaB03x" & vbCrLf & "Content-Disposition: form-data; name=""sessionid""" & vbCrLf & vbCrLf & sessionId & vbCrLf
        formData &= "--AaB03x" & vbCrLf & "Content-Disposition: form-data; name=""edituser""" & vbCrLf & vbCrLf & userId & vbCrLf
        formData &= "--AaB03x--"

        'Upload data to 
        Dim request As HttpWebRequest = WebRequest.Create(uploadTargetPage)
        request.Method = "POST"
        request.AllowAutoRedirect = True
        request.Timeout = 300 * 1000
        request.ContentType = "multipart/form-data, boundary=AaB03x"
        request.ContentLength = Encoding.GetEncoding(1252).GetBytes(formData).GetLength(0)

        'Write post data
        logFile.WriteLine(LogTimeStamp() & "Status information: Writing multipart/form-data to disk: '" & AppSettings("debugFile") & "'")
        Dim wtxt As StreamWriter = New StreamWriter(AppSettings("debugFile"), False, Encoding.GetEncoding("iso-8859-1"))
        wtxt.BaseStream.Write(Encoding.GetEncoding(1252).GetBytes(formData), 0, request.ContentLength)
        wtxt.Close()

        Try
            request.GetRequestStream().Write(Encoding.GetEncoding(1252).GetBytes(formData), 0, request.ContentLength)
        Catch
            logFile.WriteLine(LogTimeStamp() & "Upload error: Unable to open post data write stream!")
        End Try

        request.GetRequestStream().Close()

        Dim response As HttpWebResponse
        Try
            response = request.GetResponse()
        Catch
            logFile.WriteLine(LogTimeStamp() & "Upload error: Response trashed!")
        End Try

        logFile.WriteLine(LogTimeStamp() & "Upload status: " & sessionId & "/" & userId & " - " & response.StatusCode & " - " & response.StatusDescription & " - '" & response.ResponseUri.ToString & "'")

    End Sub

    Sub GenerateList(ByVal xmlList As String)

        Dim out As StreamWriter = New StreamWriter(AppSettings("tmpList"), False, Encoding.GetEncoding("iso-8859-1"))
        Dim xmlBook As XmlDocument = New XmlDocument()
        Dim xsltForm As Xsl.XslTransform = New Xsl.XslTransform()

        Try
            xmlBook.Load(xmlList)
            xsltForm.Load(AppSettings("xslTransform"))
            xsltForm.Transform(xmlBook, Nothing, out)
        Catch
            logFile.WriteLine(LogTimeStamp() & "XML error: Generate tabbed phonelist failed: '" & xmlList & "' -> '" & AppSettings("tmpList") & "'")
        End Try

        logFile.WriteLine(LogTimeStamp() & "Status information: Tabbed phonelist generated: '" & xmlList & "' -> '" & AppSettings("tmpList") & "'")
        out.Close()

    End Sub

    Sub Main()

        Dim loginStr As String

        'Logfile
        logFile = New StreamWriter(AppSettings("logFile"), True, Encoding.GetEncoding("iso-8859-1"))
        logFile.AutoFlush = True
        logFile.WriteLine(LogTimeStamp() & "Status information: Phonelist export started, exporting list to Telefonkatalogen Online")

        'First upload
        GenerateList(AppSettings("xmlPhoneList1"))

        loginStr = "?username=" & AppSettings("username1") & "&password=" & AppSettings("password1")
        Login(loginStr)
        UploadList()

        'Second upload
        GenerateList(AppSettings("xmlPhoneList2"))

        loginStr = "?username=" & AppSettings("username2") & "&password=" & AppSettings("password2")
        Login(loginStr)
        UploadList()

        logFile.WriteLine(LogTimeStamp() & "Status information: Phonelist export complete")
        logFile.Close()
        'Console.Read()
    End Sub

    Function LogTimeStamp() As String
        Return Now().ToShortDateString & " - " & Now().ToLongTimeString & "  :  "
    End Function

End Module
